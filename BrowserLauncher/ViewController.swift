  //
//  MainViewController.swift
//  BrowserLauncherUI
//
//  Created by Jussi Yli-Urpo on 20.3.16.
//  Copyright © 2016 Solipaste. All rights reserved.
//

import Cocoa
import SwiftyUserDefaults
import Regex

class ViewController: NSViewController {
  
  let altBrowser = Defaults[.altBrowser]
  let altBrowserRegex = Defaults[.altBrowserRegex]
  let defaultBrowser = Defaults[.defaultBrowser]
  
  @IBOutlet weak var defaultBrowserNameTextField: NSComboBox!
  @IBOutlet weak var altBrowserNameTextField: NSComboBox!
  @IBOutlet weak var altBrowserRegexTextField: NSTextField!
  
  @IBOutlet weak var currentHandler: NSTextField!
  @IBOutlet weak var currentHandlerHttps: NSTextField!
  
  @IBOutlet weak var setDefaultBrowserButton: NSButton!
  @IBOutlet weak var setDefaultBrowserHintLabel: NSTextField!
  
  @IBOutlet weak var appNameVersionLabel: NSTextField!
  
  override func viewDidLoad() {
    super.viewDidLoad()
    defaultBrowserNameTextField.stringValue = defaultBrowser ?? ""
    defaultBrowserNameTextField.addItemsWithObjectValues(getUrlHandlerCandidates("http"))
    
    altBrowserNameTextField.stringValue = altBrowser ?? ""
    altBrowserNameTextField.addItemsWithObjectValues(getUrlHandlerCandidates("https"))
    altBrowserRegexTextField.stringValue = altBrowserRegex ?? ""
    
    let bundle = NSBundle.mainBundle()
    appNameVersionLabel.stringValue = "\(bundle.displayName ?? "") \(bundle.releaseVersionNumber ?? "")"
  }
  
  func onResignActive() {
    Defaults[.defaultBrowser] = defaultBrowserNameTextField.stringValue
    Defaults[.altBrowser] = altBrowserNameTextField.stringValue
    let value = altBrowserRegexTextField.stringValue
    if isValidRegex(value) {
      Defaults[.altBrowserRegex] = value
    }
  }
  
  func onActive() {
    showCurrentHandler()
  }
  
  @IBAction func setAsDefault(sender: AnyObject) {
    let mainBundle = NSBundle.mainBundle()
    if let bundleId = mainBundle.bundleIdentifier {
      let httpResult = LSSetDefaultHandlerForURLScheme("http", bundleId)
      let httpsResult = LSSetDefaultHandlerForURLScheme("https", bundleId)
      // ignore errors for https
      
      if (httpResult != 0 ) {
        let alert = NSAlert()
        alert.addButtonWithTitle("OK")
        alert.messageText = "Failed with \(httpResult), \(httpsResult)"
        alert.informativeText = "something went wrong"
        alert.alertStyle = NSAlertStyle.CriticalAlertStyle
        alert.runModal()
      }
      showCurrentHandler()
      
    } else {
      NSLog("Failed to get bundle id")
    }
  }
  
  func showCurrentHandler() {
    let mainBundle = NSBundle.mainBundle()
    var isCurrent = false
    let workspace = NSWorkspace.sharedWorkspace()
    if let currentHandlerId = convertCfTypeToString(LSCopyDefaultHandlerForURLScheme("http")) {
      isCurrent = currentHandlerId == mainBundle.bundleIdentifier
      let currentHandlerAppName = workspace.URLForApplicationWithBundleIdentifier(currentHandlerId)?.lastPathComponent
//      let currentHandlerPath = workspace.absolutePathForAppBundleWithIdentifier(currentHandlerId)
//      NSLog("Current http handler: \(currentHandlerId) at \(currentHandlerPath)")
      currentHandler.stringValue = "http: " + (currentHandlerAppName ?? "")
    }
    if let currentHandlerId = convertCfTypeToString(LSCopyDefaultHandlerForURLScheme("https")) {
      let currentHandlerAppName = workspace.URLForApplicationWithBundleIdentifier(currentHandlerId)?.lastPathComponent
//      let currentHandlerPath = workspace.absolutePathForAppBundleWithIdentifier(currentHandlerId)
//      NSLog("Current https handler: \(currentHandlerId) at \(currentHandlerPath)")
      currentHandlerHttps.stringValue = "https: " + (currentHandlerAppName ?? "")
    }
    
    setDefaultBrowserButton.enabled = !isCurrent
    setDefaultBrowserHintLabel.enabled = isCurrent
  }
}

extension ViewController : NSControlTextEditingDelegate {
  
  func control(control: NSControl, textShouldEndEditing fieldEditor: NSText) -> Bool {
    if let value = fieldEditor.string where !value.isEmpty {
      switch control.tag {
      case 0:
        Defaults[.defaultBrowser] = value
      case 1:
        Defaults[.altBrowser] = value
      case 2:
        if isValidRegex(value) {
          Defaults[.altBrowserRegex] = value
        } else {
          return false
        }
      default:
        NSLog("Error control \(control.tag)")
      }
    }
    return true
  }
  
  override func controlTextDidChange(obj: NSNotification) {
    if obj.object is NSTextField {
      let control = obj.object as! NSTextField
      let value = control.stringValue
      if control.tag == 2 {
        if isValidRegex(value) {
          Defaults[.altBrowserRegex] = value
          control.drawsBackground = false
          control.backgroundColor = NSColor.whiteColor()
        } else {
          control.drawsBackground = true
          control.backgroundColor = NSColor(red: 1, green: 0, blue: 0, alpha: 0.5)
//          NSLog("Invalid regex: \(value)")
        }
      }
    }
  }
  
  func isValidRegex(regex: String) -> Bool {
    do {
      _ = try NSRegularExpression(pattern: regex, options: [])
      return true
    } catch {
      return false
    }
  }
  
  func getUrlHandlerCandidates(scheme: String) -> [String] {
    let workspace = NSWorkspace.sharedWorkspace()
    let handlerBundleIds = LSCopyAllHandlersForURLScheme(scheme)
    let handlerBundleStrings = convertCfArrayToStringArray(handlerBundleIds)
    let handlerAppNames = handlerBundleStrings?.map({(bundleId: String) -> String? in
      let s = workspace.URLForApplicationWithBundleIdentifier(bundleId)?.lastPathComponent as NSString?
      return s?.stringByDeletingPathExtension
    }).flatMap({$0}).sort()
    return handlerAppNames ?? []
  }
}

extension NSBundle {
  
  var releaseVersionNumber: String? {
    return self.infoDictionary!["CFBundleShortVersionString"] as? String
  }
  
  var displayName: String? {
    return self.infoDictionary!["CFBundleDisplayName"] as? String
  }
  
  var buildVersionNumber: String? {
    return self.infoDictionary!["CFBundleVersion"] as? String
  }
  
}
