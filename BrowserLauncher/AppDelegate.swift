//
//  AppDelegate.swift
//  BrowserLauncherUI
//
//  Created by Jussi Yli-Urpo on 18.3.16.
//  Copyright © 2016 Solipaste. All rights reserved.
//

import Cocoa

@NSApplicationMain
class AppDelegate: NSObject, NSApplicationDelegate {
  var input: String?
  let launcher = Launcher()
  
  @IBOutlet weak var window: NSWindow!
  
  func applicationWillFinishLaunching(notification: NSNotification) {
    let eventManager = NSAppleEventManager.sharedAppleEventManager()
    eventManager.setEventHandler(self, andSelector: #selector(AppDelegate.handleGetURLEvent(_:replyEvent:)), forEventClass: AEEventClass(kInternetEventClass), andEventID: AEEventID(kAEGetURL))
  }
  
  func applicationDidFinishLaunching(aNotification: NSNotification) {
    if launcher.handleInput(input) {
      // did handle input - do not show UI
      exit(0)
    }
  }
  
  func handleGetURLEvent(event: NSAppleEventDescriptor?, replyEvent: NSAppleEventDescriptor?) {
    if let aeEventDescriptor = event?.paramDescriptorForKeyword(AEKeyword(keyDirectObject)) {
      if let urlStr = aeEventDescriptor.stringValue {
        input = urlStr
//        NSLog("Received URL event with url: \(input)")
      }
    }
  }
  
  func applicationDidBecomeActive(notification: NSNotification) {
    if let controller =  NSApplication.sharedApplication().windows.first?.contentViewController as? ViewController {
      controller.onActive()
    }
  }
  
  func applicationDidResignActive(notification: NSNotification) {
    if let controller =  NSApplication.sharedApplication().windows.first?.contentViewController as? ViewController {
      controller.onResignActive()
    }
  }
  
  func applicationWillTerminate(notification: NSNotification) {
    if let controller =  NSApplication.sharedApplication().windows.first?.contentViewController as? ViewController {
      controller.onResignActive()
    }
  }
  
  func applicationShouldTerminateAfterLastWindowClosed(sender: NSApplication) -> Bool {
    return true
  }
}

