import Foundation
import SwiftyUserDefaults
import Regex

class Launcher {
  let altBrowser = Defaults[.altBrowser] ?? "Google Chrome"
  let altBrowserRegex = Defaults[.altBrowserRegex] ?? "vincit"
  let defaultBrowser = Defaults[.defaultBrowser] ?? "Firefox"
  
  func handleInput(input: String?) -> Bool {
    if input == nil {
//      NSLog("No input, using arguments")
//      NSLog("Argument count \(Process.arguments.count)")
//      for argument in Process.arguments {
//        NSLog("argument: \(argument)")
//      }
      
      if Process.arguments.count < 2 || Process.arguments[1].hasPrefix("-"){
//        NSLog("No arguments; received: \(Process.arguments)")
        return false
      } else {
        doLaunch(Process.arguments[1])
      }
      
    } else {
      doLaunch(input!)
    }
    return true
  }
  
  private func doLaunch(url: String) {
    let browserName = getPreferredBrowser(url)
//    NSLog("Launching \(url) with Browser \(browserName)")
    
    if let app = Launcher.findApp(browserName) {
      if let nsUrl = NSURL(string: url) {
        let ws = NSWorkspace.sharedWorkspace()
        ws.openURLs([nsUrl], withAppBundleIdentifier: app.bundleIdentifier, options: [], additionalEventParamDescriptor: nil, launchIdentifiers: nil)
      }

    }
  }
  
  class func findApp(appName: String) -> NSRunningApplication? {
    let workspace = NSWorkspace()
    let runningApps = workspace.runningApplications
    for app in runningApps {
      if app.localizedName == appName {
        if let appBundleId = app.bundleIdentifier {
          let apps = NSRunningApplication.runningApplicationsWithBundleIdentifier(appBundleId)
          return apps.first
        }
      }
    }
    return nil
  }
  
  class func launchAppWithUrl(app : NSRunningApplication, url: String) {
    let appCFUrl = app.executableURL! as CFURLRef
    let unmanagedAppUrl = Unmanaged<CFURL>.passUnretained(appCFUrl)
    let itemUrls: CFArray = [url] as CFArray
    let unmanagedItemsUrls = Unmanaged<CFArray>.passUnretained(itemUrls)
    var launchSpec = LSLaunchURLSpec(
      appURL: unmanagedAppUrl,
      itemURLs: unmanagedItemsUrls,
      passThruParams: nil,
      launchFlags: [.Defaults],
      asyncRefCon: nil)
    let launchStatus = LSOpenFromURLSpec(&launchSpec, nil)
    if (launchStatus != 0) {
          NSLog("Launching app failed wíth launchStatus \(launchStatus)")
    }

  }
  
  func getPreferredBrowser(url: String) -> String {
    let regex = Regex(altBrowserRegex, options: [.IgnoreCase])
    if !altBrowser.isEmpty && regex.matches(url) {
      return altBrowser
    } else {
      return defaultBrowser
    }
  }
}
