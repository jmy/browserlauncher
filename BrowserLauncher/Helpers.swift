//
//  Helpers.swift
//  BrowserLauncherUI
//
//  Created by Jussi Yli-Urpo on 20.3.16.
//  Copyright © 2016 Solipaste. All rights reserved.
//

import Foundation
func convertCfTypeToString(cfValue: Unmanaged<CFString>?) -> String? {
  
  if let cfValue = cfValue {
    let value = Unmanaged.fromOpaque(cfValue.toOpaque()).takeUnretainedValue() as CFStringRef
    if CFGetTypeID(value) == CFStringGetTypeID(){
      return value as String
    }
  }
  return nil
}

func convertCfArrayToStringArray(cfValue: Unmanaged<CFArray>?) -> [String]? {
  
  if let cfValue = cfValue {
    let value = Unmanaged.fromOpaque(cfValue.toOpaque()).takeUnretainedValue() as CFArray
    if CFGetTypeID(value) == CFArrayGetTypeID() {
      return value as NSArray as? [String]
    } 
  }
  return nil
}
