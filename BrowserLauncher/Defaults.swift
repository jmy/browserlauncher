//
//  Defaults.swift
//  BrowserLauncherUI
//
//  Created by Jussi Yli-Urpo on 19.3.16.
//  Copyright © 2016 Solipaste. All rights reserved.
//

import Foundation
import SwiftyUserDefaults

extension DefaultsKeys {
  static let altBrowser = DefaultsKey<String?>("altBrowser")
  static let altBrowserRegex = DefaultsKey<String?>("altBrowserRegex")
  static let defaultBrowser = DefaultsKey<String?>("defaultBrowser")
}